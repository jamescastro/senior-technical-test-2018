<?php

namespace Tests\Feature;

use App\Agency;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class AgencyServicesTest extends TestCase
{
    use DatabaseMigrations;

    public function setUp()
    {
        parent::setUp();
    }

    /** @test */
    public function get_the_services_that_an_agency_supplies()
    {
        $agency = factory(\App\Agency::class)->create();

        $services = factory(\App\Service::class, 3)->create()->pluck('id');

        $agency->services()->attach($services);

        $this->get('/agencies/' . $agency->id . '/services', ['Authorisation' => $this->token])
            ->assertJsonCount(3);
    }

    /** @test */
    public function add_a_service_to_an_agency()
    {
        $agency = factory(\App\Agency::class)->create();

        $services = factory(\App\Service::class, 2)->create()->pluck('id');

        $agency->services()->attach($services);

        $serviceId = factory(\App\Service::class)->create()->id;

        $test = $this->post('/agencies/' . $agency->id . '/services/' . $serviceId, [], ['Authorisation' => $this->token]);

        $comparison = Agency::where('id', $agency->id)->with('services')->first()->toArray();

        $test->assertJson($comparison);

    }

    /** @test */
    public function delete_a_service_from_an_agency()
    {
        $agency = factory(\App\Agency::class)->create();

        $services = factory(\App\Service::class, 2)->create()->pluck('id')->toArray();

        $serviceId = factory(\App\Service::class)->create()->id;

        $agency->services()->attach(array_push($services, $serviceId));

        $test = $this->delete('/agencies/' . $agency->id . '/services/' . $serviceId, [], ['Authorisation' => $this->token]);

        $comparison = Agency::where('id', $agency->id)->with('services')->first()->toArray();

        $test->assertJson($comparison);
    }
}
