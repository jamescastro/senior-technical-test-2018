<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class ServicesTest extends TestCase
{
    use DatabaseMigrations;

    public function setUp()
    {
        parent::setUp();
    }

    /** @test */
    public function get_list_of_services()
    {
        factory(\App\Service::class, 3)->create();

        $this->get('/services', ['Authorisation' => $this->token])
            ->assertJsonCount(3);
    }

    /** @test */
    public function get_service_details_by_slug()
    {
        $service = factory(\App\Service::class)->create();

        $this->get('/services/' . $service->slug, ['Authorisation' => $this->token])
            ->assertJson($service->toArray());
    }

}
