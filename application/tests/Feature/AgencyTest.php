<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class AgencyTest extends TestCase
{
    use DatabaseMigrations;

    public function setUp()
    {
        parent::setUp();
    }

    /** @test */
    public function get_a_list_of_all_agencies()
    {
        factory(\App\Agency::class, 3)->create();

        $this->get('/agencies', ['Authorisation' => $this->token])
            ->assertJsonCount(3);
    }

    /** @test */
    public function get_agency_details()
    {
        $agency = factory(\App\Agency::class)->create();

        $this->get('/agencies/' . $agency->id, ['Authorisation' => $this->token])
            ->assertJson($agency->toArray());
    }

    /** @test */
    public function create_agency_and_receive_the_created_agency_as_the_response()
    {
        $agency = factory(\App\Agency::class)->make();

        $this->post('/agencies', $agency->toArray(), ['Authorisation' => $this->token])
            ->assertJson($agency->toArray());

        $this->assertDatabaseHas('agencies', $agency->toArray());
    }

    /** @test */
    function a_agency_requires_a_name()
    {
        $agency = factory(\App\Agency::class)->make(['agency_name' => null]);

        $this->post('/agencies', $agency->toArray(), ['Authorisation' => $this->token])
            ->assertJsonValidationErrors('agency_name');
    }

    /** @test */
    function a_agency_requires_a_contact_email()
    {
        $agency = factory(\App\Agency::class)->make(['contact_email' => null]);

        $this->post('/agencies', $agency->toArray(), ['Authorisation' => $this->token])
            ->assertJsonValidationErrors('contact_email');
    }

    /** @test */
    function a_agency_requires_a_web_address()
    {
        $agency = factory(\App\Agency::class)->make(['web_address' => null]);

        $this->post('/agencies', $agency->toArray(), ['Authorisation' => $this->token])
            ->assertJsonValidationErrors('web_address');
    }

    /** @test */
    function a_agency_requires_a_short_desciption()
    {
        $agency = factory(\App\Agency::class)->make(['short_description' => null]);

        $this->post('/agencies', $agency->toArray(), ['Authorisation' => $this->token])
            ->assertJsonValidationErrors('short_description');
    }

    /** @test */
    function a_agency_requires_an_established_date()
    {
        $agency = factory(\App\Agency::class)->make(['established' => null]);

        $this->post('/agencies', $agency->toArray(), ['Authorisation' => $this->token])
            ->assertJsonValidationErrors('established');
    }

}
