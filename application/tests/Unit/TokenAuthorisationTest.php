<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class TokenAuthorisationTest extends TestCase
{
    use DatabaseMigrations;

    public function setUp()
    {
        parent::setUp();
    }

    /** @test */
    public function unauthorised_access_to_services_route_protected_by_token_auth()
    {
        $this->get('/agencies')
            ->assertStatus(401);
    }

    /** @test */
    public function get_authentication_token_from_the_post_request_and_validate_it()
    {

        $this->get('/agencies', ['Authorisation' => $this->token])
            ->assertStatus(200);

    }
}
