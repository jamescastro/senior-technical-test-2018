<?php

namespace Tests;

use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Illuminate\Support\Facades\DB;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;

    public $token = 'QSxmMZJ9mkowr00xecaLHOWgTMKtAQsAvqh4XxDBiBwywiyBH3Ddo4ZTu98Z3LoW';

    public function setUp()
    {
        parent::setUp();

        DB::table('auth_tokens')->insert(['auth_token' => bcrypt($this->token)]);
    }
}
