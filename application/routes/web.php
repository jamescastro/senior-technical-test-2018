<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('agencies', 'AgencyController@index')->middleware('auth.api');
Route::get('agencies/{agency}', 'AgencyController@show')->middleware('auth.api');
Route::post('agencies', 'AgencyController@store')->middleware('auth.api');

Route::get('services', 'ServiceController@index')->middleware('auth.api');
Route::get('services/{slug}', 'ServiceController@show')->middleware('auth.api');

Route::get('agencies/{agency}/services', 'AgencyServiceController@index')->middleware('auth.api');
Route::post('agencies/{agency}/services/{service}', 'AgencyServiceController@store')->middleware('auth.api');
Route::delete('agencies/{agency}/services/{service}', 'AgencyServiceController@destroy')->middleware('auth.api');