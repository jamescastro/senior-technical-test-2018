<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Agency extends Model
{
    protected $guarded = [];

    public function services()
    {
        return $this->belongsToMany(Service::class);
    }

    protected $hidden = ['created_at', 'updated_at'];
}
