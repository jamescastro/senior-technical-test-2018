<?php
namespace App\Services;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class SimpleAPIAuth implements AuthenticateInterface {

    /**
     * var string
     */
    protected $token;

    /**
     * @param Request $request
     * @return bool
     */
    public function handle(Request $request) :bool
    {
        $this->getTokenFromRequestHeader($request);

        return $this->validateTokenFromDatabase();

    }

    /**
     * validated that the hashed token in the database matches the token received.
     *
     * @return bool
     */
    protected function validateTokenFromDatabase()
    {
        return Hash::check($this->token, DB::table('auth_tokens')->first()->auth_token);
    }

    /**
     * Get the token from the header.
     *
     * @param $request
     */
    protected function getTokenFromRequestHeader($request)
    {
        $this->token = $request->header('Authorisation');
    }
}