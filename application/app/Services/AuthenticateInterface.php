<?php
namespace App\Services;

use Illuminate\Http\Request;

interface AuthenticateInterface {

    /**
     * Authenticate the route
     *
     * @param Request $request
     * @return mixed
     */
    public function handle(Request $request) :bool;

}