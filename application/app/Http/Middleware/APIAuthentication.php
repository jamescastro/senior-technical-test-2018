<?php

namespace App\Http\Middleware;

use Closure;
use App\Services\SimpleAPIAuth;
use App\Services\AuthenticateInterface;

class APIAuthentication
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if(!$this->authenticated(new SimpleAPIAuth(), $request)){
            return response()->json('You\'re not authorised to access this API', 401);
        }

        return $next($request);
    }

    /**
     * Authenticate the token.
     *
     * @param AuthenticateInterface $auth
     * @param $request
     * @return bool|mixed
     */
    protected function authenticated(AuthenticateInterface $auth, $request)
    {
        return $auth->handle($request);
    }
}
