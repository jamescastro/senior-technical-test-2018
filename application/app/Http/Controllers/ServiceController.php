<?php

namespace App\Http\Controllers;

use App\Service;

class ServiceController extends Controller
{
    /**
     * Display a list of services.
     *
     * @return string
     */
    public function index()
    {
        return Service::all()->toJson();
    }


    /**
     * Display the specified service.
     *
     * @param  \App\Service $service
     * @param $slug
     * @return \Illuminate\Http\Response
     */
    public function show(Service $service, $slug)
    {
        return $service->where('slug', $slug)->first()->toJson();
    }

}
