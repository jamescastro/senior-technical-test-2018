<?php

namespace App\Http\Controllers;

use App\Agency;
use App\Service;

class AgencyServiceController extends Controller
{
    /**
     * Retrieve an agencies services
     *
     * @param Agency $agency
     * @return mixed
     */
    public function index(Agency $agency)
    {
        return $agency->services;
    }

    /**
     * Add the selected service to an agency
     *
     * @param Agency $agency
     * @param Service $service
     * @return Agency
     */
    public function store(Agency $agency, Service $service)
    {
        $agency->services()->attach($service->id);

        Return $agency->load('services');
    }

    /**
     * Delete the selected service from the agency.
     *
     * @param Agency $agency
     * @param Service $service
     * @return Agency
     */
    public function destroy(Agency $agency, Service $service)
    {
        $agency->services()->detach($service->id);

        Return $agency->load('services');
    }
}
