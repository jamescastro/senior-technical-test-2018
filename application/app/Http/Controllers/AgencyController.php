<?php

namespace App\Http\Controllers;

use App\Agency;
use Illuminate\Http\Request;
use Validator;

class AgencyController extends Controller
{
    /**
     * Display a list of the agencies.
     *
     * @return string
     */
    public function index()
    {
        return Agency::all()->toJson();
    }

    /**
     * Create a new agency.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'agency_name' => 'required',
            'contact_email' => 'required|email',
            'web_address' => 'required|url',
            'short_description' => 'required',
            'established' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()]);
        }

        return Agency::create($request->all())->toJson();
    }

    /**
     * Display the specified agency.
     *
     * @param  \App\Agency  $agency
     * @return string
     */
    public function show(Agency $agency)
    {
        return $agency->toJson();
    }

}
