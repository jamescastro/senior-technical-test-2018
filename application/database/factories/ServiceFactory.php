<?php

use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(App\Service::class, function (Faker $faker) {
    $name = $faker->words(2, true);

    return [
        'service_name' => $name,
        'slug' => Str::slug($name)
    ];
});
