<?php

use Faker\Generator as Faker;

$factory->define(App\Agency::class, function (Faker $faker) {
    return [
        'agency_name' => $faker->company,
        'contact_email' => $faker->companyEmail,
        'web_address' => $faker->url,
        'short_description' => $faker->sentence,
        'established' => $faker->year
    ];
});