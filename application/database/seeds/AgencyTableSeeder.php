<?php

use App\Agency;
use Illuminate\Database\Seeder;

class AgencyTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $agency1 = Agency::create(['agency_name' => 'RoRo\'s Rocket Chips', 'contact_email' => 'hello@roro.com', 'web_address' => 'http://roro.com', 'short_description' => 'The fieriest chips known to man.', 'established' => 2019]);
        $agency2 = Agency::create(['agency_name' => 'Heavy Profesh Web Dev', 'contact_email' => 'us@greatdevs.biz', 'web_address' => 'https://greatdevs.biz', 'short_description' => 'The most professional developers in town.', 'established' => 1994]);
        $agency3 = Agency::create(['agency_name' => 'Shass Kinsalott', 'contact_email' => 'sounds@shasskinsal.ot', 'web_address' => 'https://shasskinsal.ot', 'short_description' => 'Post-modern audio branding agency based in London.', 'established' => 2000]);

        $agency1->services()->attach([1,2]);
        $agency2->services()->attach([1,3]);
        $agency3->services()->attach([2,3]);
    }
}
