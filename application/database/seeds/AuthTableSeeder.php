<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AuthTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $token = 'QSxmMZJ9mkowr00xecaLHOWgTMKtAQsAvqh4XxDBiBwywiyBH3Ddo4ZTu98Z3LoW';

        DB::table('auth_tokens')->insert(['auth_token' => bcrypt($token)]);
    }
}
